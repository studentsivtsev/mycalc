package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions;

public class CalcException extends Exception {
    public CalcException(Throwable e){
        super(e);
    }
    public CalcException(){
        super();
    }
}
