import junit.framework.TestCase;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.MyCalc;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.commands.PlusOp;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.commands.PrintOp;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

public class PlusOpTest extends TestCase {

    final static double VALUE_OF_a = 4;

    private final Map<Map.Entry<Double,Object>, Double> toInvokeData = new HashMap<>();

    protected void setUp() throws Exception{

        Map<Double,Object> map = new HashMap<>();

        //first
        map.put(1.,"a");
        toInvokeData.put(map.entrySet().iterator().next(), 1. + VALUE_OF_a);

        //second
        map.put(2.,3.);


        for (Map.Entry<Double,Object> it : map.entrySet() ){
            if (it.getKey() != 1.){
                toInvokeData.put(it, it.getKey() + (double) it.getValue());
            }
        }

    }

    protected void tearDown() throws Exception{

    }

    public void testInoke() throws Exception{
        setUp();

        TreeMap<String,Double> definitions = new TreeMap<>();
        definitions.put("a", VALUE_OF_a);

        ArrayList<String> plusCommandLine = new ArrayList<>();
        plusCommandLine.add("+");
        Stack<Double> stack = new Stack<>();
        PlusOp plusOp = new PlusOp();
        PrintOp printOp =  new PrintOp();

        for (Map.Entry<Double, Object> it:toInvokeData.keySet()){
            stack.push(it.getKey());
            if (it.getValue().equals("a")) {
                stack.push(VALUE_OF_a);
            }
            else{
                stack.push((double) it.getValue());
            }
            MyCalc.MyContext context = new MyCalc.MyContext(stack, definitions);


            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            //PrintStream ps = new PrintStream()
            Writer writer = new OutputStreamWriter(outStream);

            plusOp.invoke(context, plusCommandLine, writer);
            printOp.invoke(context, plusCommandLine, writer);
            //writer.flush();

            double d = Double.parseDouble(outStream.toString());


            assertTrue(toInvokeData.get(it).equals(d));

            stack.clear();
        }

        tearDown();
    }
}
