package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Properties;

public class CommFactory {

    private static CommFactory cf = null;
    private static Properties confTable = new Properties();

    public static CommFactory getInstance() {
        if(cf == null ) {
            cf = new CommFactory();
        }
        return cf;
    }

    private CommFactory() {
        try {
            confTable.load(getClass().getResourceAsStream("conf"));
        }
        catch (IOException e) {
            System.err.println("Something went wrong while loading conf");
        }
    }

    public Command getCommand(String commandName){

        Reader reader = new InputStreamReader(getClass().getResourceAsStream("conf"));
        //Reader reader = new FileInputStream("conf"); Почему не так??
        Command newCommand = null;
        try {
            if (confTable.containsKey(commandName)) {
                newCommand = (Command) Class.forName(confTable.getProperty(commandName)).newInstance();
            }
        }
        catch (ClassNotFoundException e){
            System.err.println("Operation class file cannot be found");
        }
        catch (IllegalAccessException | InstantiationException e){
            System.err.println("Something went wrong while opening operation class file");
        }

        return newCommand;
    }
}
