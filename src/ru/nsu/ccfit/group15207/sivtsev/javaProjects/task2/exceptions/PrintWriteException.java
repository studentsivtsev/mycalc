package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions;

public class PrintWriteException extends CalcException{
    @Override
    public String getLocalizedMessage() {
        return "Something went wrong while writing node";
    }
    public PrintWriteException(Throwable e){
        super(e);
    }
    public PrintWriteException(){
        super();
    }
}
