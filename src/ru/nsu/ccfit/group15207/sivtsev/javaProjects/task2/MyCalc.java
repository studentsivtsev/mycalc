package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2;

import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions.CalcException;

import java.io.*;
import java.util.ArrayList;
import java.util.Stack;
import java.util.TreeMap;

public class MyCalc {

    private MyContext calcContext;
    public MyContext getContext(){
        return (calcContext);
    }

    private Writer writer;

    public Writer getWriter() {
        return writer;
    }
    public void setWriter(Writer wr){
        writer = wr;
    }

    MyCalc(){
        calcContext = new MyContext();
        writer = new OutputStreamWriter(System.out);
    }

    public void run(Reader reader){
        ArrayList<ArrayList<String>> comms2 = MyParser.parseFromReader2(reader);

        for (ArrayList<String> it : comms2) {
            Command command = CommFactory.getInstance().getCommand(it.get(0));
            if (command != null) {
                try {
                    command.invoke(this.getContext(), it, this.getWriter());
                } catch (CalcException e) {
                    System.err.println(e.getLocalizedMessage());
                }
            }
        }
    }

    public static class MyContext {

        private Stack<Double> stack;
        private TreeMap<String, Double> map;

        public MyContext() {
            stack = new Stack<>();
            map = new TreeMap<>();
        }

        public MyContext(Stack<Double> stck, TreeMap<String, Double> defs) {
            stack = stck;
            map = defs;
        }

        public int stackSize(){
            return stack.size();
        }

        public Stack<Double> getStack() {
            return stack;
        }

        public TreeMap<String, Double> getMap() {
            return map;
        }

        public Double putToMap(String def, Double val) {
            return map.put(def, val);
        }

        public Double pushToStack(Double node) {
            return stack.push(node);
        }

        public Double popToStack() {
            return stack.pop();
        }

        public Double peekFromStack() {
            return stack.peek();
        }

        public Double dPeekFromStack(){
            double d = stack.pop();
            double p = stack.peek();
            stack.push(d);
            return p;
        }
    }

}
