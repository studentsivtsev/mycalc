package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions;

public class StackException extends CalcException{
    public StackException(Throwable e){
        super(e);
    }
    public StackException(){
        super();
    }
}
