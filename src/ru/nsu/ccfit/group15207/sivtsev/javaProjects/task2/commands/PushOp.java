package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.commands;

import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.Command;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.MyCalc;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.MyParser;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions.CalcException;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions.NotEnoughParException;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions.NotValidParException;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions.TooManyParException;

import java.io.Writer;
import java.util.ArrayList;

public class PushOp implements Command {
    public void invoke(MyCalc.MyContext context, ArrayList<String> commandLine, Writer writer) throws NotValidParException{
        if (commandLine.size() < 2){
            throw new NotEnoughParException();
        }
        if (commandLine.size() > 2){
            throw new TooManyParException();
        }
        if (context.getMap().containsKey(commandLine.get(1))){
            context.pushToStack(context.getMap().get(commandLine.get(1)));
        }
        else {
            context.pushToStack(MyParser.gentlyParseDouble(commandLine.get(1)));
        }
    }
}
