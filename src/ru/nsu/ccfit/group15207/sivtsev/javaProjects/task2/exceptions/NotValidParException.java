package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions;

public class NotValidParException extends CalcException{ //надо ли его делать абстрактным?
    public NotValidParException(Throwable e){
        super(e);
    }
    public NotValidParException(){
        super();
    }
}
