package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2;



import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions.CalcException;

import java.io.*;
import java.util.ArrayList;

public class Main {
    //private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String args[]) {
        Logger logger = LogManager.getLogger(Main.class);


        logger.info("Ssss");


        MyCalc calculator = new MyCalc();

        Reader reader;

        if (args.length == 1) {
            String fileName = args[0];
            reader = getReader(fileName);
        } else {
            reader = new InputStreamReader(System.in);
        }

        calculator.run(reader);

        closeCloseable(reader);
    }


    static public Reader getReader(String fileName) {

        Reader reader;

        try {
            reader = new InputStreamReader(new FileInputStream(fileName));
            return reader;
        } catch (FileNotFoundException e) {
            System.err.println("Something went wrong while opening file: " + e.getLocalizedMessage());

            System.err.println("Working with console. Insert \"" + MyParser.STOP_SYMB + "\" to finish");
            reader = new InputStreamReader(System.in); // working with console

        }

        return reader;
    }


    static public int closeCloseable(Closeable file) {

        if (file != null) {
            try {
                file.close();
            } catch (IOException e) {
                System.err.println("Something went wrong while closing file: " + e.getLocalizedMessage());
            }
        }
        return 0;
    }
}
