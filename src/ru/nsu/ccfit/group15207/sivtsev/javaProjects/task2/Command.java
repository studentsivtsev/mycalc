package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2;

import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions.CalcException;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions.NotValidParException;

import java.io.Writer;
import java.util.ArrayList;

public interface Command {
    void invoke(MyCalc.MyContext context, ArrayList<String> commandLine, Writer writer) throws CalcException;
}
