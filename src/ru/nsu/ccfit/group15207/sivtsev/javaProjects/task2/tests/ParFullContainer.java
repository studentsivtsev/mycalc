package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.tests;

import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.MyCalc;

import java.util.ArrayList;

public class ParFullContainer {
    public MyCalc.MyContext context;
    public ArrayList<String> stringList;
    public ParFullContainer(MyCalc.MyContext c, ArrayList<String> s){
        context = c;
        stringList = s;
    }
}
