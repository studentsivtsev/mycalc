package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions;

public class EmptyCalcStackException extends StackException{
    @Override
    public String getLocalizedMessage() {
        return "Calculator stack is SO-O-O-O empty";
    }
    public EmptyCalcStackException(){
        super();
    }
}
