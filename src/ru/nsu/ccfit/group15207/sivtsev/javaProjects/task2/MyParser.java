package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2;

import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions.FallingParseException;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.LinkedList;

public abstract class MyParser {

    final static char STOP_SYMB = 'q';

    static public ArrayList<ArrayList<String>> parseFromReader2(Reader reader) {

        ArrayList<ArrayList<String>> list = new ArrayList<>();


        int i;

        try {
            StringBuilder s = new StringBuilder();
            do {
                ArrayList<String> opString = new ArrayList<>();
                do {

                    i = reader.read();
                        if ((char) i == ' ') {
                            opString.add(s.toString());
                            s.delete(0, s.length());
                        }
                        if ((char) i != '\n' && (char) i != ' ' && (char) i != STOP_SYMB && i != -1) {
                            s.insert(s.length(), (char) i);
                        }
                } while ((char) i != '\n' && (char) i != STOP_SYMB &&  i != -1);

                if (s.length() != 0) {
                    opString.add(s.toString());
                }
                s.delete(0, s.length());
                if (opString.size() != 0) {
                    list.add(opString);
                }
            }while(i != -1 && (char)i != STOP_SYMB);
        }
        catch (IOException e){
            System.err.println("Something went wrong while reading file: " + e.getLocalizedMessage());
        }
        return list;
    }

    static public double gentlyParseDouble(String s) throws FallingParseException{
        double d;
        try {
            d = Double.parseDouble(s);
        }
        catch (NumberFormatException e){
            throw new FallingParseException(e);
        }
        return d;
    }

    /*static public LinkedList<String> parseFromReader(Reader reader) {

        LinkedList<String> list = new LinkedList<>();


        int i;

        try {
            StringBuilder s = new StringBuilder();
            do {

                i = reader.read();


                if ((char) i != ' ' && (char) i != '\n' && i != -1){
                    s.insert(s.length(),(char) i);
                }
                else{
                    list.add(s.toString());
                    s.delete(0,s.length());

                }
            }while(i != -1 && (char)i != '.');
        }
        catch (IOException e){
            System.err.println("Something went wrong while reading file: " + e.getLocalizedMessage());
        }
        return list;
    }*/
}

