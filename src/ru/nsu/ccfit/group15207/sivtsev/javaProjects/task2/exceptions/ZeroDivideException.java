package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions;

public class ZeroDivideException extends ArithmeticalExceprtion{
    @Override
    public String getLocalizedMessage() {
        return "Attempt to divide by zero";
    }
}
