import junit.framework.TestCase;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.MyCalc;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.commands.DefineOp;

import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.*;

public class DefineOpTest extends TestCase {

    private final Map<TreeMap<String,Double>, TreeMap<String,Double>> toInvokeData= new HashMap<>();

    protected void setUp() throws Exception{

        //the first
        TreeMap<String,Double> inMap1 = new TreeMap<>();
        TreeMap<String,Double> resultMap1 = new TreeMap<>();
        resultMap1.putAll(inMap1);
        resultMap1.put("a",2.);
        toInvokeData.put(resultMap1, inMap1);

        //the second
        TreeMap<String,Double> inMap2 = new TreeMap<>();
        inMap2.putAll(inMap1); //вторую мапу нарочно делаем из первой, чтоб проверить перезапись
        TreeMap<String,Double> resultMap2 = new TreeMap<>();
        resultMap2.putAll(inMap2);
        resultMap2.put("a",4.);
        toInvokeData.put(resultMap2,inMap2);
    }

    protected void tearDown() throws Exception{
        toInvokeData.clear();
    }

    public void testInvoke() throws Exception{
        //тест добавления пары  "a" - 2.0 и ее перезаписи в "a" - (2.0 + 2*(k-1)) в последующем k-ом разу в definitions с помощью DefineOp.invoke()
        setUp();

        Stack<Double> newStack = new Stack<>();

        ArrayList<String> pushCommandLine = new ArrayList<>();
        pushCommandLine.add("DEFINE");
        pushCommandLine.add("a");

        DefineOp defineOp = new DefineOp();

        double value = 2.;

        for (TreeMap<String,Double> it :toInvokeData.keySet() ){
            MyCalc.MyContext inContext = new MyCalc.MyContext(newStack,toInvokeData.get(it));
            pushCommandLine.add(((Double)value).toString());
            Writer writer = new OutputStreamWriter(System.out);
            defineOp.invoke(inContext,pushCommandLine, writer);
            assertTrue(areMapsEquals(inContext.getMap(), toInvokeData.get(it)));

            value += 2;
            pushCommandLine.remove(2);
        }


        tearDown();
    }

    public boolean areMapsEquals(TreeMap<String,Double> a, TreeMap<String,Double> b){
        if (a.size() != b.size()){
            return false;
        }
        Iterator it2 = b.keySet().iterator();
        if (a.size() != 0) {
            for (String it : a.keySet()) {
                String str = it2.next().toString();
                if (!it.equals(str))
                    return false;
                if (!a.get(it).equals(b.get(str)))
                    return false;
            }
        }
        return true;
    }
}