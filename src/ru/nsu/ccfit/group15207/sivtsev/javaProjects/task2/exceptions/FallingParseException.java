package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions;

public class FallingParseException extends NotValidParException{
    @Override
    public String getLocalizedMessage() {
        return "Invalid par. Number is required";
    }

    public FallingParseException(Throwable e) {
        super(e);
    }
}
