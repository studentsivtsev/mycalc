package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.commands;

import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.Command;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.MyCalc;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions.*;

import java.io.Writer;
import java.util.ArrayList;

public class PlusOp implements Command{
    public void invoke(MyCalc.MyContext context, ArrayList<String> commandLine, Writer writer) throws CalcException{
        if (commandLine.size() < 1){
            throw new NotEnoughParException();
        }
        if (commandLine.size() > 1){
            throw new TooManyParException();
        }
        if (context.stackSize() == 0) {
            throw new EmptyCalcStackException();
        }
        if (context.stackSize() == 1) {
            throw new NotEnoughNodeException();
        }
        context.pushToStack(context.peekFromStack() + context.dPeekFromStack());
    }
}
