//package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.src.tests;

import junit.framework.TestCase;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.MyCalc;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.commands.PushOp;

import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.*;

public class PushOpTest extends TestCase {

    final static double VALUE_OF_a = 4;

    private final Map<Stack<Double>, Stack<Double>> toInvokeData = new HashMap<>();

    protected void setUp() throws Exception{

        //that correct first test
        Stack<Double> inStack1 = new Stack<>();
        Stack<Double> resultStack1 = new Stack<>();
        resultStack1.push((double) 2);
        toInvokeData.put(inStack1,resultStack1);
        //

        //that super correct second test
        Stack<Double> inStack2 = new Stack<>();
        inStack2.push(2.);
        Stack<Double> resultStack2 = new Stack<>();
        resultStack2.push(2.);
        resultStack2.push(VALUE_OF_a);
        toInvokeData.put(inStack2,resultStack2);

    }
    protected void tearDown() throws Exception{
        toInvokeData.clear();
    }

    public void testInvoke() throws Exception {
        //тест добавления "2" - в первый и "a" в последующие разы в стек с помощью PushOp.invoke()


        setUp();

        TreeMap<String,Double> definitions = new TreeMap<>();
        definitions.put("a", VALUE_OF_a);

        ArrayList<String> pushCommandLine = new ArrayList<>();
        pushCommandLine.add("PUSH");

        int ctr = 0;
        PushOp pushOp = new PushOp();

        for (Stack<Double> it: toInvokeData.keySet()){
            Stack<Double> newStack = new Stack<>();
            newStack.addAll(it);
            MyCalc.MyContext inContext = new MyCalc.MyContext(newStack,definitions);
            if (ctr == 0){
                pushCommandLine.add("2");
                ++ctr;
            }
            else{
                pushCommandLine.remove("2");
                pushCommandLine.add("a");
            }
            Writer writer = new OutputStreamWriter(System.out);
            pushOp.invoke(inContext, pushCommandLine, writer);
            assertTrue(areStacksEquals(inContext.getStack(), toInvokeData.get(it)));

        }


        tearDown();
    }
    public boolean areStacksEquals(Stack<Double> a, Stack<Double> b){
        if (a.size() != b.size()){
            return false;
        }
        Iterator it2 = b.iterator();
        if (a.size() != 0){
            for (Double it : a){
                if (it != (double)it2.next())
                    return false;
            }
        }
        return true;
    }
}