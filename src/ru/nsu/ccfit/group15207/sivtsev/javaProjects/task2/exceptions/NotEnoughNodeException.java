package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions;

public class NotEnoughNodeException extends StackException{
    @Override
    public String getLocalizedMessage() {
        return "There aren't enough numbers for the operation";
    }
    public NotEnoughNodeException(){

    }
}
