package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions;

public class NegativeSqRtException extends ArithmeticalExceprtion{
    @Override
    public String getLocalizedMessage() {
        return "Attempt to SqRt a negative number";
    }
}
