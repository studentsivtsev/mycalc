package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.commands;

import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.Command;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.MyCalc;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.MyParser;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions.NotEnoughParException;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions.NotValidParException;
import ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions.TooManyParException;

import java.io.Writer;
import java.util.ArrayList;

public class DefineOp implements Command{
    public void invoke(MyCalc.MyContext context, ArrayList<String> commanLine, Writer writer) throws NotValidParException{
        if (commanLine.size() < 3){
            throw new NotEnoughParException();
        }
        if (commanLine.size() > 3){
            throw new TooManyParException();
        }
        context.putToMap(commanLine.get(1), MyParser.gentlyParseDouble(commanLine.get(2)));

    }
}
