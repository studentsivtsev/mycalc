package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions;

public class TooManyParException extends NotValidParException{
    @Override
    public String getLocalizedMessage() {
        return "Too many par";
    }
    public TooManyParException(Throwable e){
        super(e);
    }
    public TooManyParException(){
        super();
    }
}
