package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task2.exceptions;

public class NotEnoughParException extends NotValidParException {
    @Override
    public String getLocalizedMessage() {
        return "Not enough par";
    }
    public NotEnoughParException(Throwable e){
        super(e);
    }
    public NotEnoughParException(){
        super();
    }
}
